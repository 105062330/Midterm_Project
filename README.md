# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Topic Name]
* Key functions (add/delete)
    1. [Membership Mechanism]
    2. [GitLab Page]
    3. [Database]
    4. [RWD]
    5. [Topic Key Function]
* Other functions (add/delete)
    1. [Third-Party Sign In]
    2. [Chrome Notification]
    3. [Use CSS Animation]
    4. [Security Report]
    5. [Other functions]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
我一共有三個html, 第一個是開始畫面, 有背景影片以及開始按鈕, 我將背景影片認為是other function是因為這個動作比較麻煩, 需要利用jquery和bootstrap才能運作, 同時也要考慮RWD, 所以我在視窗<1000的時候用了將影片的長和寬都要重新調整, 而按下start按鈕後, 就會跑到第二個html, 是登入畫面. 登入畫面可以基本的sign in, sign up, 也可以用第三方的登入像是google或facebook, 這裡我上課時的lab06幫了我許多. 而在這個頁面裡也有採用css animation, 當滑鼠移到輸入或是login按鈕時, 會往外擴張已表示目前所在位置. 這頁的template參考https://colorlib.com/wp/html5-and-css3-login-forms/. 而在成功登入進去之後, 會有chrome notificatoin通知登入成功, 同時會跳到第三個html也就是聊天室. 聊天室可以正常聊天, load歷史紀錄, 也可以跟新的使用者聊天, 也可以多人聊天, 只要有登入進去, 一開始預設顯示的名字則是第三方登入的名字, 像是創建google帳號時取的名字或是facebook的名字, 想改的話在下方修改即可. 左上方有個menu, 裡面會有你當前的帳號信箱, 登出按鈕, 還有關於作者資訊. 當你登出成功後, 會有chrome notification通知登出成功, 並且同時跳到開始畫面的html.聊天室的template則是參考http://bin.webduino.io/budiw/2/edit?html,js,output. 而關於RWD, 三個html裡我給的寬度都是device的寬度, 所以在你用不同大小的裝置使用時, 可以符合裝置的大小使用, 只需往下拉不需左右滑動, 而我的字體也會隨著寬度做調整, 整個比例才會看起來相似.

## Security Report (Optional)
在gitlab網頁上打開後是https協議, 即是安全的網站. https採用TLS和SSL協議, SSL透過憑證交換來驗證客戶端或伺服器端, 在傳輸前交換只有雙方知道的溝通金鑰. 當資料要傳輸時, 傳送方會使用溝通金鑰為訊息加密, 因為只有接收端握有相同的溝通金鑰, 所以即使訊息被駭客擷取, 也只能獲得無法解譯的亂碼. 溝通金鑰也有可能會被破解或重播, 但在SSL機制中, 每次重新交易時都會亂數產生一組新的溝通金鑰, 即使駭客取得前次的溝通金鑰也無用武之地, 從而保障了網站的安全. TLS則提供 1.加密: 對交換的資料進行加密, 防止資料遭到竊取. 也就是說, 當使用者在瀏覽網站時, 任何人都無法「竊聽」其對話, 追蹤他們在多個網頁之間轉換的活動, 或竊取其資訊. 2.資料完整性: 系統會偵測出資料在傳輸過程中是否遭到有意或無意的修改或破壞. 3. 驗證: 驗證您的使用者是否與預期的網站進行通訊. 這能預防攔截式攻擊並建立使用者的信任感, 進而提升安全性.

