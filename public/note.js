document.addEventListener('DOMContentLoaded', function () {
    if (!Notification) {
        alert('Desktop notifications not available in your browser. Try Chromium.');
        return;
    }

    if (Notification.permission !== "granted")
        Notification.requestPermission();
});

function notifyMe() {
    if (Notification.permission !== "granted")
        Notification.requestPermission();
    else {
        var notification = new Notification('Chat Room', {
            icon: 'images/icon.png',
            body: "Login In Successfully",
        });
        notification.onclick = function () {
            window.location.replace('chat.html');
        };
    }
}

function notifyLogout() {
    if (Notification.permission !== "granted")
        Notification.requestPermission();
    else {
        var notification = new Notification('Chat Room', {
            icon: 'images/icon.png',
            body: "Logout Successfully",
        });
        notification.onclick = function () {
            window.location.replace('chat.html');
        };
    }
}

function newMessage() {
    if (Notification.permission !== "granted")
        Notification.requestPermission();
    else {
        var notification = new Notification('Chat Room', {
            icon: 'images/icon.png',
            body: "You have new message",
        });
        notification.onclick = function () {
            window.location.replace('about.html');
        };
    }
}