//jQuery is required to run this code
$( document ).ready(function() {

    scaleVideoContainer();

    initBannerVideoSize('.video-container .poster img');
    initBannerVideoSize('.video-container .filter');
    initBannerVideoSize('.video-container video');

    $(window).on('resize', function() {
        scaleVideoContainer();
        scaleBannerVideoSize('.video-container .poster img');
        scaleBannerVideoSize('.video-container .filter');
        scaleBannerVideoSize('.video-container video');
    });

});

function scaleVideoContainer() {

    var height = $(window).height() + 5;
    var unitHeight = parseInt(height) + 'px';
    $('.homepage-hero-module').css('height',unitHeight);

}

function initBannerVideoSize(element){

    $(element).each(function(){
        $(this).data('height', $(this).height());
        $(this).data('width', $(this).width());
    });

    scaleBannerVideoSize(element);

}

function scaleBannerVideoSize(element){

    var windowWidth = $(window).width(),
    windowHeight = $(window).height() + 5,
    videoWidth,
    videoHeight;

    // console.log(windowHeight);

    $(element).each(function(){
        var videoAspectRatio = $(this).data('height')/$(this).data('width');

        $(this).width(windowWidth);

        if(windowWidth < 1000){
            videoHeight = windowHeight;
            videoWidth = videoHeight / videoAspectRatio;
            $(this).css({'margin-top' : 0, 'margin-left' : -(videoWidth - windowWidth) / 2 + 'px'});

            $(this).width(videoWidth).height(videoHeight);
        }

        $('.homepage-hero-module .video-container video').addClass('fadeIn animated');

    });
}

$(document).ready(function(){
  
    var vid = $('video');
    var vid_w_orig = 1280;
      var vid_h_orig = 720;
  
    // re-scale image when viewport resizes
      $(window).resize(function(){
      
          // get the parent element size
          var container_w = vid.parent().width();
          var container_h = vid.parent().height();
      
          // use largest scale factor of horizontal/vertical
          var scale_w = container_w / vid_w_orig;
          var scale_h = container_h / vid_h_orig;
          var scale = scale_w > scale_h ? scale_w : scale_h;
      
          // scale the video
          vid.width(scale * vid_w_orig);
          vid.height(scale * vid_h_orig);
      
      });
    
    // trigger re-scale on pageload
      $(window).trigger('resize');
    
  });

function signin(){
    window.location="signin.html";
}